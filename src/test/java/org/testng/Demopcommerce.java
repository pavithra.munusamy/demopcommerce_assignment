package org.testng;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import net.bytebuddy.build.Plugin.Factory.UsingReflection.Priority;

public class Demopcommerce {

	  WebDriver driver;
	  @BeforeClass
	  void launch() {
	  driver =new ChromeDriver();
	  driver.manage().window().maximize();
	  
	 
  }
  
   @Test(priority = 1)
  public void Tc1() {
	  driver.get("https://admin-demo.nopcommerce.com/login");
	  WebElement email = driver.findElement(By.id("Email"));
	  email.clear();
	  email.sendKeys("admin@yourstore.com");
	  WebElement password = driver.findElement(By.name("Password"));
	  password.clear();
	  password.sendKeys("admin");
	  driver.findElement(By.xpath("//button[text()='Log in']")).click();
	   
     
      String actual= driver.findElement(By.linkText("John Smith")).getText();
      String expected="John Smith";
      Assert.assertEquals(actual, expected);
		
	 
  }
   
   
   @Test(priority=2)
   public void Tc2() throws InterruptedException, BiffException, IOException {
	   
	   driver.findElement(By.xpath("//i[@class='nav-icon fas fa-book']")).click();
	   driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	   driver.findElement(By.xpath("//a[@href='/Admin/Category/List']")).click();
	   driver.findElement(By.partialLinkText("Add new")).click();
	   
	   File f=new File("/home/pavithra/Documents/Demopcommerce.xlsx");
		FileInputStream fis=new FileInputStream(f);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheetAt(0);
		int rows=sheet.getPhysicalNumberOfRows();
		for(int i=1;i<rows;i++)
			
		{
			
				String name=sheet.getRow(i).getCell(0).getStringCellValue();
				String description=sheet.getRow(i).getCell(1).getStringCellValue();
				//String pricefrom = sheet.getRow(i).getCell(2).getStringCellValue();
				//String priceto = sheet.getRow(i).getCell(3).getStringCellValue();
			
	   
	 	 //  driver.findElement(By.xpath("//input[@id='Name']")).sendKeys("John Smith");
	   
	 //  driver.findElement(By.id("Description_ifr")).sendKeys("description");
				driver.findElement(By.id("Name")).sendKeys(name);
				driver.findElement(By.id("Description_ifr")).sendKeys(description);
				
				
	   WebElement ele = driver.findElement(By.xpath("//select[@class='form-control valid']"));
	   Select sel=new Select(ele);
	   sel.selectByVisibleText("Computers >> Build your own computer");
	   
	   WebElement price = driver.findElement(By.xpath("//label[@for=\"PriceFrom\"]"));
		
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView()",price);
		
		//driver.findElement(By.id("PriceFrom")).sendKeys(pricefrom);
		//driver.findElement(By.id("PriceTo")).sendKeys(priceto);
		 WebElement savebtn = driver.findElement(By.xpath("//button[@name='save']"));
		 js.executeScript("arguments[0].scrollIntoView(true)", savebtn);
		 savebtn.click();
		 driver.findElement(By.id("SearchCategoryName")).sendKeys("JohnKrease");
         driver.findElement(By.id("search-categories")).click();
			
                	
        /* WebElement element = driver.findElement(By.xpath("(//td[contains(text(),'JohnKrease')])[1]"));
         String text=element.getText();
         Assert.assertEquals("JohnKrease", text);*/
		
		}	
}
		
		
		@Test(priority=3)
		public void Tc3() {
	
			 driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
			  driver.findElement(By.partialLinkText("Catalog")).click();
			
			  driver.findElement(By.xpath("//p[contains(text(),'Products')]")).click();
			  driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
			  driver.findElement(By.id("search-products")).click();
			 /* WebElement element = driver.findElement(By.xpath("//td[text()='Build your own computer']"));
		      String text=element.getText();
		      Assert.assertEquals("Build your own computer", text);*/
			
		}
		
		  @Test(priority=4)
		  public void Tc4() throws IOException {
			  driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
			  driver.findElement(By.partialLinkText("Catalog")).click();
			  driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		      driver.findElement(By.xpath("//p[contains(text(),'Manufacturers')]")).click();
		            driver.findElement(By.partialLinkText("Add new")).click();
		      File f= new File("/home/pavithra/Documents/Demopcommerce.xlsx");
		      FileInputStream fis=new FileInputStream(f);
				XSSFWorkbook workbook=new XSSFWorkbook(fis);
				XSSFSheet sheet=workbook.getSheetAt(1);
				int rows=sheet.getPhysicalNumberOfRows();
				for(int i=1;i<rows;i++)
				{
					String name=sheet.getRow(i).getCell(0).getStringCellValue();
					String description=sheet.getRow(i).getCell(1).getStringCellValue();
		          driver.findElement(By.id("Name")).sendKeys(name);
		          driver.findElement(By.id("Description_ifr")).sendKeys(description);
		          driver.findElement(By.xpath("//button[@name='save']")).click();
		          driver.findElement(By.id("SearchManufacturerName")).sendKeys("Shell");
		          driver.findElement(By.id("search-manufacturers")).click();
		         /* WebElement element = driver.findElement(By.xpath("//td[text()='Shell']"));
		          String text=element.getText();
		          Assert.assertEquals("Shell", text);*/
				}
		  }
		
		 
		
		
		  @AfterClass
		  public void closebrowser() {
			  WebDriver driver=new ChromeDriver();
			  driver.close();
		  
		}




	   
	   
   }


	
  
 

